service rabbitmq-server stop 
service rsyslog stop
service haproxy stop
service kz-whistle_apps stop
service kz-ecallmgr stop
service kamailio stop
service httpd stop
service bigcouch stop

service bigcouch restart
service rabbitmq-server restart 
service rsyslog restart
service haproxy restart
service kz-whistle_apps restart
service kz-ecallmgr restart
service kamailio restart
service httpd restart
